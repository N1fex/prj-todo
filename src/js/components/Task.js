﻿import $ from "jquery";
import progbar from "progressbar.js";
import updateHead from './updateHead'

export default class Task{
  constructor(task){
    this.id = task.id;
    this.title = task.title;
    this.completed = task.completed;
  }

  render(){
    return `
      <div class="todo__task task" id=${this.id}>
        <div class="task__main active">
          <div class="task__window">
            <div class="task__info info">
              <span class="info__title">${this.title}</span>
            </div>
            <div class="task__buttons buttons">
              <i class="fa fa-trash buttons__remove" aria-hidden="true"></i>
              <i class="fa fa-pencil-square-o buttons__edit" aria-hidden="true"></i>
            </div>
          </div>
          <button class="task__state ${this.completed ? 'green' : 'red'}">${this.completed ? 'Completed' : 'Pending'}</button>
          <div class="task__progressbar progressbar"></div>        
        </div>
        <div class="task__wedit wedit">
          <div class="wedit__title title-wedit">
            <span>Title</span>
            <input type="text" class="title-wedit__title" value="${this.title}">
          </div>
          <button class="wedit__close">Close X</button>
        </div>
      </div>
    `
  }

  functionality(){
    circles['task-'+this.id] = new progbar.Circle($('.task#'+this.id+ ' .progressbar')[0],{
      color: this.randColor(),
      duration: 2000,
      easing: 'easeInOut',
      strokeWidth: 8,
      trailColor: '#dfe8ed',
      trailWidth: 8,
      text: {
        value: 0
      }
    });

    circles['task-'+this.id].text.style.fontSize = '12px';
    circles['task-'+this.id].animate(!!this.completed,{
      step: () => {
        circles['task-'+this.id].setText(Math.round(circles['task-'+this.id].value()*100))
      }
    });

    // кнопка состояния таска
    this.eventListener(this.id, '.task__state', this.compFunc);
    // кнопка удаления таска
    this.eventListener(this.id, '.buttons__remove', this.removeFunc);
    // кнопка редактирования таска
    this.eventListener(this.id, '.buttons__edit', this.editFunc);
    // кнопка закрытия wedit
    this.eventListener(this.id, '.wedit__close', this.closeWedit);
  }

  eventListener(id, selector, func){
    $(`#${id} ${selector}`).on('click', func.bind(this));
  }

  compFunc() {
    let idx = tasks.findIndex(task => task.id === this.id);
    tasks[idx].completed = !tasks[idx].completed;

    $('.task#' + this.id + ' .task__state')
      .removeClass(tasks[idx].completed ? 'red' : 'green')
      .addClass(tasks[idx].completed ? 'green' : 'red')
      .text(tasks[idx].completed ? 'Completed' : 'Pending');

    circles['task-'+this.id].animate(!!tasks[idx].completed,{
      step: () => {
        circles['task-'+this.id].setText(Math.round(circles['task-'+this.id].value()*100))
      }
    });

    updateHead();
  }

  removeFunc() {
    tasks = tasks.filter(task => task.id !== this.id);

    let newCircles = [];
    Object.keys(circles).map(key => {
      if(key !== 'task-'+this.id) newCircles[key] = circles[key];
    });
    circles = newCircles;

    $('.task#'+this.id).remove();
    updateHead();
  }

  editFunc(){
    this.toggleEditFunc('openWedit');
  }

  closeWedit(){
    let title = $('.task#'+this.id+' .wedit .title-wedit__title').val();
    let idx = tasks.findIndex(task => task.id === this.id);
    tasks[idx].title = title;
    $('.task#'+this.id+' .info__title').text(title);

    this.toggleEditFunc('closeWedit');
  }

  toggleEditFunc(str){
    let removeTarget, openTarget;
    if(str === 'openWedit'){
      removeTarget = '.task__main';
      openTarget = '.wedit';
    } else {
      removeTarget = '.wedit';
      openTarget = '.task__main';
    }

    $('.task#'+this.id+' '+removeTarget).removeClass('active');
    $('.task#'+this.id+' '+openTarget).addClass('active');
  }

  randColor(){
    let r = Math.floor(Math.random() * (256)),
      g = Math.floor(Math.random() * (256)),
      b = Math.floor(Math.random() * (256));
    return '#' + r.toString(16) + g.toString(16) + b.toString(16);
  }
}




